# Notities van een gesprek met Mike, mijn buurman, dierenarts en veearts.

> Sensorstickers: Printable electronics die PV-cellen en sensors met elkaar combineert in één compacte unit.

## Medisch onderzoek
Het monitoren van patiënten kan moeilijk op afstand gebeuren. Patiënten moeten meestal binnen de omgeving van het ziekenhuis of laboratorium blijven. Metingen worden gedaan door op vaste tijdstippen samples te nemen. Door de gecontroleerde omgeving zijn de gegevens die hier uit voortkomen niet altijd toepasbaar in de praktijk. Met sensorstickers zou een patient op afstand monitort kunnen worden in real-time terwijl hij/zij door kan gaan met het normale leven.

Sensorstickers worden op de huid aangebracht op strategische plekken. Voor ECG (hartslag meting) worden er 3 stickers geplakt op de borst. Voor het meten van bloeddruk en lichaamstemperatuur wordt de oorlel gebruikt. Voor sensors die alleen werken binnen het lichaam, bijvoorbeeld een bloedsuiker sensor, worden met een chip geïnjecteerd. De sticker op de huid fungeert als stroombron die de via contactloos opladen de chip voorziet van stroom. Ook zit er een antenne en/of geheugen in de sticker gebouwd die de sensordata opslaat en verzend.

Dit zou ook gebruikt worden in de training van atleten. Normaal gesproken kunnen atleten alleen in een labomgeving uitgemeten worden. Deze stickersensors maken het mogelijk om metingen te doen in de eigen omgeving van de atleet.