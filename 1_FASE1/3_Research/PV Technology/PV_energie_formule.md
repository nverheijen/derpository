# Formule voor berekenen output zonnecellen
E = A * r * H * PR

E = energie output in Kilowattuur (kWh)
A = totale oppervlakte zonnecellen in vierkante meter (m^2)
r = efficiëntie van de zonnecel in procenten %
    S-ci:                24%
    CIGS:                20%
    Huidige OPV cel:     8%
    Nabije toekomst OPV: 16%
H = jaarlijkse zonnestraling per vierkante meter (kWh/m².y)
    Daken in Nederland:  1025
    Binnenshuis:         0 tot 72
PR= ratio van energieverlies (0.5 tot 0.9, default = 0.75)
    Energie die verloren gaat door inverters, temperatuur, bekabeling, schaduwen, stof, krassen, enz.


## Voorbeeld
Berekenen van de oppervlakte voor het voorzien van een klein e-paper scherm.

E = onder actief gebruik het scherm 1.6 watt per uur
A = onbekend
r = OPV cel 8%
H = 72 binnenshuis
PR= 0.75 normaal energieverlies

1.6 = A * 8 * 72 * 0.75

> http://www.wolframalpha.com/input/?i=1.6+%3D+A+*+8+*+72+*+0.75

A = 0.0037037 m^2 = 37.037 cm^2
Vierkant paneel van 6.09x6.09 cm

## Bronnen
> http://www.sciencedirect.com/science/article/pii/S0038092X06002362
> http://solargis.info/doc/free-solar-radiation-maps-GHI
> http://photovoltaic-software.com/PV-solar-energy-calculation.php
> https://en.wikipedia.org/wiki/Cadmium_telluride_photovoltaics#mediaviewer/File:Best_Research-Cell_Efficiencies.png